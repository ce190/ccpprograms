/*Write programs to print the following patterns using appropriate programming constructs

6. AAA
BBB
CCC
*/
#include<stdio.h>

int main()
{
    int i, j;
    for (i = 1; i <= 3; i++)
    {
        for (j = 1; j <= 3; j++)
         printf("%c",(i+64));
        printf("\n");
    }
    return 0;
}
