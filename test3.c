//Develop a C program to enter a number and calculate the sum of its digits.
#include<stdio.h>

int main() {
    int num, sum = 0, remainder = 0;
    printf("Enter the number you want the sum of");
    scanf("%d", &num);
    while (num != 0) 
	{

        remainder = num % 10;


        sum=sum+remainder;

        num = num / 10;

    }
    printf("The sum of your digits is %d", sum);
}
