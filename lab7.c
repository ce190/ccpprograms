//Develop a program to find average of n numbers.(using arrays)
#include<stdio.h>
int main()
{
    int a[100],num,i;
    float sum=0;
    float average;
    printf("How many numbers average do you want to find? ");
    scanf("%d",&num);
    for (i=0;i<num;++i)
    {
    printf("%d. Enter number ",i+1);
    scanf("%d",&a[i]);
    sum=sum+a[i];
    }
    average=sum/num;
    printf("The average is %.3f ",average);
}
