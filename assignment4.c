//Write a program to calculate the roots of a quadratic equation with using switch .
#include<stdio.h>
#include<math.h> 

int main()
{
	float a, b, c, root1, root2, imaginary, disc;

	printf("Enter values of a, b, c of quadratic equation : ");
	scanf("%f%f%f", &a, &b, &c);


	disc= (b * b) - (4 * a * c);


	switch(disc > 0)
	{
		case 1:

			root1 = (-b + sqrt(disc)) / (2 * a);
			root2 = (-b - sqrt(disc)) / (2 * a);

			printf("Two distinct and real roots exists: %.2f and %.2f", 
				   root1, root2);
			break;

		case 0:
			switch(discriminant < 0)
			{
				case 1:

					root1 = root2 = -b / (2 * a);
					imaginary = sqrt(-discriminant) / (2 * a);

					printf("Two distinct complex roots exists: %.2f + i%.2f and %.2f - i%.2f", 
						   root1, imaginary, root2, imaginary);
					break;

				case 0:
					root1 = root2 = -b / (2 * a);

					printf("Two equal and real roots exists: %.2f and %.2f", root1, root2);

					break;
			}
	}

	return 0;
}
