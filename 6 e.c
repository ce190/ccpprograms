5. Write a program to check whether the given number is palindrome or not.
*/
#include<stdio.h>
int main()
{
    int num,remainder,reverse=0,originalNum;
    printf("Enter your number ");
    scanf("%d",&num);
    originalNum=num;
    while (num!=0)
    {
        remainder=num%10;
        num=num/10;
        reverse=reverse*10;
        reverse=reverse + remainder;
    }
    if (originalNum==reverse)
    {   
        printf("The number is palindrome ");
    }
    else 
    {
        printf("The number is not palindrome");
    }
}
